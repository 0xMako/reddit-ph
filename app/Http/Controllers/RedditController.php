<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RedditController extends Controller
{
    public function index()
    {
        $url = "https://api.reddit.com/user/the_yaya/submitted.json?limit=1";
        $client = new \GuzzleHttp\Client([
            'headers' => ['User-Agent' => 'testing/1.0'],
            'verify' => false]);
        $response = $client->request('GET', $url);
        $content = json_decode($response->getBody(), true);
        $id = $content['data']['children'][0]['data']['id'];

        return redirect('https://redd.it/'.$id);
    }
}
